package com.agibank.assis.anderson.Testevaga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestevagaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestevagaApplication.class, args);
	}

}
